# Friedas Mond

![Friedas Mond picture](pictures/DSC05006.JPG?raw=true "Friedas Mond")

A night light that reacts to sound.

## Parts

* yellow moon night light
* ESP8226 ESP01
* [ESPEasy release mega-20181208](https://github.com/letscontrolit/ESPEasy/releases/tag/mega-20181208)
* voltage regulator input: DC 5V, output: DC 3.3V (eg. LM2569)
* Neopixel WS2812B strip (8 LEDs)
* Mikrofon Sound Sensor Modul (eg. KY-038)
* 3x 10kΩ resistor
* USB connector or USB cable (to connect to USB power)
* cables, circuit board, breadboard - whatever to use for connecting the parts

## Versions

v1 - first version: very simple, but works. Drawback: all LEDs are the same resulting in coarse steps in brightness especially when the light is low

v2 - new version: LEDs are used as four pairs. Each pair does change its brightness. Smooth change in brightness especially when the light is low. Drawback: rules are slow and each change needs around 2200ms resulting in 3 second steps. Slow.

## Installation

* Make sure your voltage regulator has an output of 3.3V when connected to your power source.
* Connect your parts - look at [FriedasMond_Steckplatine.png](pictures/FriedasMond_Steckplatine.png?raw=true "FriedasMond_Steckplatine.png") (or use the fritzing file in fritzing).
* Install [ESPEasy](https://www.letscontrolit.com/wiki/index.php/Basics:_Connecting_and_flashing_the_ESP8266) on an ESP8226
* After configuring your WLAN and password on tools tab use Settings/Load to upload config.dat
* Now there should be a tab named Rules. If not goto Tools/Advanced and enable 'Rules' and 'Old Engine'.
* In tab Rules fillin 'Rules Set 1' with the content of rules1.txt and 'Rules Set 2' with the content of rules2.txt
* Done.

## Setup (v2)

* The Mikrofon Sound Sensor Modul needs to be adjusted so that the output is not triggered while normal noise (snoring ;-) ) is around, but is well triggered when louder unusual sound is around. In tab Devices you can watch Task 3 (Generic Pulse Counter - noise) while you try: The device counts pulses from the Mikrofon Sound Sensor Modul for 10 seconds and then puts them into delta. total contains the totally counted pulses and time the time used to collect the totally counted pulses. The latter both are not used in rules - only delta is used.
* The color of the LEDs is determined by Task 4 (Dummy - color). You can change the multipliers which is used to calculate the brightness of each color red, green, blue from Task 7 (Dummy, led_ctrl) brightness.
* When noise is detected and extends a given value Task 5 (Dummy, state) level is rised by a fixed amount. You can find the values in tab Rules looking for the part beginning with 'on noise#delta>30 do'. The Values can be changed as needed, but shouldn't exceed 254.
* Task 7 (Dummy, state) treshold and max_brightness can be adjusted. treshold is subtracted from level and can be used to extend the time noise is needed before the LEDs start to switch on. max_brightness can be used to limit the brightness each LED can reach.
* There is no need to change any values to get the setup running. It sould work with the configuration provided.
